
9 topics:

1. Predictive Analytics Problems and Tools

	a. understand the different types of predictive modeling problems

	b. Write and execute basic commands in R using RStudio


2. Topic: Problem Definition

	a. Translate a vague question into one that can be analyzed with statistics and predictive analytics to solve a business problem.

	b. Consider factors such as available data and technology, significance of business impact, and implementation challenges to define the problem.


3. Topic: Data Visualization

	a. Understand the key principles of contructing graphs

	b. Create a variety of graphs using the ggplot2 package


4. Topic: Data Types and Exploration

	a. Identify structured, unstructured, and semi-structured data.

	b. Identify the types of variables and terminology used in predictive modeling.

	c. Understand basic methods of handling missing data.

	d. Implement effective data design with respect to time frame, sampling, and granularity.

	e. Apply univariate and bivariate data exploration techniques.


5. Topic: Data Issues and Resolutions

	a. Evaluate the quality of appropriate data sources for a problem.

	b. Identify opportunities to create features from the basic data that may add value.

	c. Identify outliers and other data issues.

	d. Handle non-linear relationships via transformations.

	e. Identify the regulations, standards, and ethics surrounding predictive modeling and data collection.


6. Topic: Generalized Linear Models

	a. Implement ordinary least squares regression in R and understand model assumptions.

	b. Understand the specifications of the GLM and the model assumptions.

	c. Create new features appropriate for GLMs

	d. Interpret model coefficients, interaction terms, offsets, and weights.

	e. Select and validate a GLM appropriately

	f. Explain the concepts of bias, variance, model complexity, and bias-variance trade-off

	g. Select appropriate hyperparameters for reguarized regression.


7. Topic: Decision Trees

	a. Understand the basic motivation behind decision trees.

	b. Construct regression and classification trees

	c. Use bagging and random forests to improve accuracy.

	d. Use boosting to improve accuracy.

	e. Select appropriate hyperparameters for decision trees and related techniques.


8. Topic Cluster and Principal Component Analyses

	a. Understand and apply K-means clustering

	b. Understand and apply hierarchical clustering

	c. Understanding and apply principal component analysis


9. Topic Communication

	a. Develop and justify a recommended analytics solution

	b. Communicate in a clear and straightforward manner using common language that is appropriate for the intended audience.

	c. Structure a report in an effective manner.

	d. Follow standards of practice for actuarial communication.

