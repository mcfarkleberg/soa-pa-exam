PA Youtube videos


What is PA?

SOA PA Exam

- What's on the exam

	- 9 topics

		Problems and Tools

		Problem Definition

		Data Visualization

		Data Types and Exploration

		Data Issues and Resolutions

		Topic: Generalized Linear Models

		Topic: Decision Trees

		Topic: Cluster and Principal Component Analysis

		Topic: Communication

- What is this exam?

	bit of an odd ball compared to the other exams out there

	project format (5 hours)

	Use R/Excel/Word


- What do they want you to know

	how to take garbage data, make inferences and write a report.


- How will i cover the material?

	1. Statistics Background
		GLMs
		Decision Trees
		Cluster and Principal Component Analysis

	2. Programming Background

		General R
		Package Overview
		Dataframes
		GGPlot2
		??
		GLMs in R
		Decision Trees in R
		Cluster and Principal Component Analysis in R

	3. Communication

		What the SOA wants

		Writing a good report

	4. Data Exploration

	5. Data Cleaning

	4. Kaggle Compeitions as Practice

	5. Previous Exams as Practice



















